# IntrestDigest (A Nodejs Pintrest Clone)
A FreeCodeCamp [Backend Challenge](https://www.freecodecamp.com/challenges/build-a-pinterest-clone)

### Usage
1. Fill in details in .env (You can use the ones provided, but they are periodicaly changed and shut down)
2. Edit the partial views files with the details and branding of the site
3. Install Modules `npm install`
4. If you want, you can run with nodemon `sudo npm install -g nodemon && npm run NodemonStart`

### Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

### To-Do
* Add Boards
* Add Private Pins

### User Storys
* As an unauthenticated user, I can login with Twitter.
* As an authenticated user, I can link to images.
* As an authenticated user, I can delete images that I've linked to.
* As an authenticated user, I can see a Pinterest-style wall of all the images I've linked to.
* As an unauthenticated user, I can browse other users' walls of images.
* As an authenticated user, if I upload an image that is broken, it will be replaced by a placeholder image. (can use jQuery broken image detection)

###### Credits
Boilerplate: [here](http://megaboilerplate.com/)
Written by [adamisntdead](http://adamisntdead.com) || [github](http://github.com/adamisntdead)
