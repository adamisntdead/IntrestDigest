var Pin = require('../models/Pin');

function isURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' + // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
    '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
  return pattern.test(str);
}

// CREATE NEW Pin
exports.postNew = function(req, res) {
  var pin = new Pin();

  if (!isURL(req.body.imageUrl)) {
    req.flash('error', {
      msg: 'The URL You Entered Is Not Valid'
    });
    return res.redirect('/new');
  }

  pin.creatorId = req.user._id;
  pin.caption = req.body.caption;
  pin.imageUrl = req.body.imageUrl;
  // NOTE (todo) - Add Private Pin Feature
  pin.privateBool = false;

  pin.save(function(err) {
    if (err) console.log(err);

    res.redirect('/dash');
  });
};

// GET NEW PIN PAGE
exports.getNew = function(req, res) {
  res.render('new', {
    title: 'Add Pin'
  });
};

// GET A USERS PINS
exports.getUser = function(req, res) {
  Pin.find({
    creatorId: req.params.userId,
    privateBool: false
  }, function(err, pins) {
    if (err) {
      req.flash('error', {
        msg: 'User Does Not Exist / Has No Pins'
      });
      return res.render('user', {
        title: 'User: ' + req.params.userId + '\'s Pins',
        userId: req.params.userId
      });
    }

    else {
      res.render('user', {
        title: 'User: ' + req.params.userId + '\'s Pins',
        pins: pins,
        userId: req.params.userId
      });
    }
  });
};


// GET A MY PINS
exports.getMy = function(req, res) {
  Pin.find({
    creatorId: req.user._id
  }, function(err, pins) {
    if (err) {
      req.flash('error', {
        msg: 'No Pins Found'
      });
      return res.render('user', {
        title: 'My Pins',
        myaccount: true
      });
    }

    else {
      res.render('user', {
        title: 'My Pins',
        myaccount: true,
        pins: pins
      });
    }
  });
};

exports.deletePin = function(req, res) {
  Pin.find({
    _id: req.params.id, creatorId: req.user._id}).remove().exec(function(err, data) {
    res.redirect('/mypins');
  });
};