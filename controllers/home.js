var Pin = require('../models/Pin');

/**
 * GET /
 */
exports.index = function(req, res) {
  if (req.user) {
    res.redirect('/dash');
  }
  else {
    Pin.find({privateBool: false}, function(err, pins) {
      console.log(pins);
      res.render('home', {
        title: 'Home',
        pins: pins
      });
    });
  }
};

exports.dash = function(req, res) {
  Pin.find({privateBool: false}, function(err, pins) {
    console.log(pins);
    res.render('home', {
      title: 'Dash',
      pins: pins
    });
  });
}
