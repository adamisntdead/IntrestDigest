var mongoose = require('mongoose');

var schemaOptions = {
  timestamps: true,
  toJSON: {
    virtuals: true
  }
};

// NOTE (todo) Add Boards To Schema

var pinSchema = new mongoose.Schema({
  creatorId: String,
  caption: String,
  privateBool: Boolean,
  imageUrl: String
});

var Pin = mongoose.model('Pin', pinSchema);

module.exports = Pin;
